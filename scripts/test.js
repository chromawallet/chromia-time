const { spawn, exec } = require('child_process');
const { exit } = require('process');
const kill = require('tree-kill');
const path = require('path');
const http = require('http');
const fs = require('fs-extra');
const os = require('os');

const binariesBaseUrl = 'http://www.chromia.dev/rellr';
const binariesFilename = 'rellr-0.10.3-ver-3.3.0-dist.tar.gz';
const rellVersion = '0.10.3';

const dir = path.join(os.homedir(), '.rell', 'bin');
const outputDir = path.join(dir, rellVersion);
const file = path.join(dir, binariesFilename);

const testResults = [];

async function untar(file, outputDir) {
    await execAsync(`tar xvzf ${file} -C ${outputDir}`)
}

async function execAsync(command) {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                reject(error);
            } else {
                resolve([stdout, stderr]);
            }
        });
    });
}

function downloadBinaries() {
	return new Promise(async (resolve, reject) => {
        if (await fs.pathExists(dir) === false) {
            await fs.ensureDir(dir);
        }

        if (await fs.pathExists(outputDir) === false) {
            await fs.ensureDir(outputDir);
        }

		http.get(`${binariesBaseUrl}/${binariesFilename}`, response => {
            const code = response.statusCode;
            if (code >= 400) {
                reject();
            } else {
                response
                    .pipe(fs.createWriteStream(file))
                    .on('finish', () => resolve())
                    .on('error', () => {
                        if (fs.existsSync(file)) {
                            fs.unlinkSync(file)
                        }
                        reject();
                    });
            }
        })
        .on('error', reject);
	});
}

async function downloadBinariesIfNeeded() {
    if (fs.pathExistsSync(outputDir)) { return }

    console.log('Cannot find Postchain binaries.');
    console.log('Starting binaries download ...');
    await downloadBinaries();
    console.log('Extracting the binaries ...');
    await untar(file, outputDir);
    console.log('Done!')
}

function startNode() {
    console.log('Starting Postchain node ...');

    const commandPath = path.join(outputDir, 'postchain-node', 'multirun.sh')

    const sourceDir = path.join(__dirname, '..', 'rell', 'src');
    const configPath = path.join(__dirname, '..', 'rell', 'config', 'run_tests.xml');

    const child = spawn(commandPath, [
        '-d', sourceDir,
        configPath
    ]);
    
    child.stdout.on('data', function(data){
        // used to filter out postchain logs
        const currentYear = new Date().getFullYear().toString();

        let output = data.toString();
        if (output.match(/.*POSTCHAIN APP STARTED.*/)) {
            console.log("Postchain node ready!");
            child.emit('ready');
        } else if (
            !output.startsWith(currentYear)
                ||
            output.match(/.*Failed to append transaction.*/)
        ) {
            output = output.endsWith('\n') 
                ? output.substring(0, output.length - 1) 
                : output;
            console.log(output);
        }
    });
    
    child.stderr.on('data', function(data){
        let output = data.toString();
        output = output.endsWith('\n') 
            ? output.substring(0, output.length - 1) 
            : output;

        console.log(output);
    });
    
    child.stdin.on('data', function(data){
        console.log('stdin:'+data);
    });

    return child;
}

function startTests() {
    console.log("Starting tests ...");
    const tests = spawn('node', [path.join(__dirname, 'test-lib.js')]);

    tests.stdout.on('data', function(data){
        const testOutputs = data.toString()
            .split('\n')
            .map(line => line.match(/\.*({.*"_type":.*}).*/))
            .filter(match => match && match.length > 0)
            .map(match => match[1])

        for(const testOutput of testOutputs) {
            const output = JSON.parse(testOutput);

            if (output._type === 'start') {
                console.log(`Starting ${output.test} ...`);
            } else if (output._type === 'end') {
                if (output.result === 'success') {
                    console.log(`Test ${output.test} \x1b[32mPASSED\x1b[0m`);
                } else {
                    console.log(`Test ${output.test} \x1b[31mFAILED\x1b[0m`);
                }

                tests.emit('test-executed', { test: output.test, result: output.result });
            } else if(output._type === "info") {
                console.log(`INFO: ${output.message}`)
            }
        }        
    });

    tests.stderr.on('data', function(data) {
        console.log(data.toString());
    });

    return tests;
}

function printResults(testResults) {
    const failedTests = testResults.filter(test => test.result === 'fail');

    console.log('');
    console.log(`\x1b[33mExecuted ${testResults.length} tests\x1b[0m`);
    console.log('\x1b[33m-------------------------------------\x1b[0m');
    console.log(`\x1b[32mPASSED: ${testResults.length - failedTests.length}\x1b[0m`);
    console.log(`\x1b[31mFAILED: ${failedTests.length}\x1b[0m`);
    failedTests.forEach(({ test }) => console.log(`\x1b[31m${test}\x1b[0m`));
    console.log('');
}

async function runTests() {
    await downloadBinariesIfNeeded();

    const node = startNode();
    
    node.on('exit', () => {
        exit();
    });
    
    node.on('ready', () => {
        const tests = startTests();
    
        tests.on('exit', () => {
            printResults(testResults);
            // kill(node.pid);
            console.log(node.pid);
        })

        tests.on('test-executed', result => {
            testResults.push(result);
        })
    });

    process.on('SIGINT', () => {
        kill(node.pid);
    });

    process.on('uncaughtException', e => {
        console.log('Uncaught Exception...');
        console.log(e.stack);
        kill(node.pid);
    });
}

(async () => {
    await runTests();
})()
