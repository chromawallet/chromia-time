const {
    op,
    Postchain,
    KeyPair
} = require('ft3-lib');

const {
    util
} = require('postchain-client');

const constValues = [];
function getByteArrayParameter(name) {
    if(name.startsWith("const_")) { 
        // if it's a constant value, check if we have it already stored
        if(constValues[name]) return constValues[name];
        else { // otherwise, create new value and store it
            const res = handleByteArrayParameter(name.split(/const_(.+)/)[1]);
            constValues[name] = res;
            return res;
        }
    } 
    return handleByteArrayParameter(name);
    
}
function handleByteArrayParameter(name) {
    switch(true) {
        case name.startsWith("signer"): {
            const keyPair = new KeyPair();
            return {
                type: 'signer',
                arg: keyPair.pubKey,
                keyPairs: [keyPair]
            };
        }
        case name.startsWith("pubkey"): {
            const keyPair = new KeyPair();
            return {
                type: 'pubkey',
                arg: keyPair.pubKey
            };
        }
        case name.startsWith("random"): {
            return {
                type: 'byte_array',
                arg: util.randomBytes(32)
            };
        }
        default: throw new Error(`Don't know how to handle byte_array parameter: ${name}`);
    }
}

function handleByteArrayListParameter(name, count) {
    if (name.startsWith('signer')) {
        const keyPairs = Array(count).fill().map(() => new KeyPair());
        return {
            type: 'signer',
            arg: keyPairs.map(({ pubKey }) => pubKey),
            keyPairs: keyPairs
        };
    } else if(name.startsWith('pubkey')) {
        const keyPairs = Array(count).fill().map(() => new KeyPair());
        return {
            type: 'pubkey',
            arg: keyPairs.map(({ pubKey }) => pubKey)
        }
    } else if(name.startsWith('random')) {
        return {
            type: 'byte_array',
            arg: Array(count).fill().map(() => util.randomBytes(32))
        }
    } else {
        throw new Error(`Don't know how to handle list<byte_array> parameter: ${name}`)
    }
}

function handleCollectionParameter(name, type) {
    switch (type.type) {
        case 'list': return handleListParameter(name, type.value);
    }
}
function handleListParameter(name, type) {
    const match = name.match(/^.+_(\d+)$/);

    if (!match || match.length != 2) {
        throw new Error("Invalid parameter name. Cannot read item count for list parameter");
    }

    const count = +match[1];

    switch (type) {
        case 'byte_array': return handleByteArrayListParameter(name, count);
    }
}

function handleParameter(parameter) {
    const { name, type } = parameter;

    if (typeof type === 'object') {
        return handleCollectionParameter(name, type);
    }

    switch(type) {
        case 'byte_array': return getByteArrayParameter(name);
        case 'list': return handleListParameter(name, type);
        default: throw new Error(`Don't know how to handle ${type} parameter: ${name}`)
    }
}

function prepareOperation(operationName, parameters) {
    if (parameters.length === 0) {
        return [[], op(operationName)];
    } else {
        const arguments = [];

        for (const parameter of parameters) {
            arguments.push(handleParameter(parameter));
        }

        return [
            arguments.filter(({ type }) => type === 'signer').flatMap(({ keyPairs }) => keyPairs ), 
            op(operationName, ...arguments.map(({ arg }) => arg ))
        ];
    }
}

module.exports.runModuleTests = async function(module_name, url, brid) {
    const blockchain = await new Postchain(url).blockchain(brid);

    let response = await blockchain.query("rell.get_app_structure", {});
    console.log("RESPONSE:", response);
    let tests = [];
    
    tests.push(response.modules[module_name].operations);

    for (const operations of tests.reverse()) {
        for (const operation_name of Object.keys(operations)) {
            console.log(`{ "_type":"start", "test":"${operation_name}" }`);
            const [signers, operation] = prepareOperation(operation_name, operations[operation_name].parameters)
            try {
                const transaction = blockchain.transactionBuilder()
                    .add(operation)
                    .build(signers.map(({ pubKey }) => pubKey));
                
                signers.forEach(signer => transaction.sign(signer));
                
                await transaction.post();

                console.log(`{ "_type":"end", "test":"${operation_name}", "result":"success" }`);

            } catch (error) {
                console.log(`{ "_type":"end", "test":"${operation_name}", "result":"fail" }`);
            }
        }
    }
}