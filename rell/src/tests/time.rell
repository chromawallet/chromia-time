import t: ^.lib.time.base_time;
import lib.time.abstrack.definition.{
	date,
	datejust,
	time,
	
	new_datejust,
	new_date,
	new_time
};
import lib.time.test.{
	expect_date,
	expect_datejust,
	expect_timestamp
};

import lib.test.{
	expect_integer
};




operation test_to_timestamp_pre_70s() {
	val timestamp = t.to_timestamp(new_datejust(new_date(1932, 12, 26)));
	print("TIMESTAMP", timestamp * t.utils.SECONDS_IN_A_DAY );
	require(timestamp == -1168128000);
}

operation test_to_timestamp_era_4() { //day of year
	
	// prior era 5
	val chron = t.to_timestamp(new_datejust(new_date(1994, 9, 24)));
	print("timestamp", chron * t.utils.SECONDS_IN_A_DAY);
	require(chron == 780364800);
}

operation test_to_timestamp_era_5() { //day of year

	// with era 5	
	val date = date(year = 2020, monthIdx = 2, dayIdx = 15);
	val chron = t.to_timestamp(new_datejust(date));
	print(chron * t.utils.SECONDS_IN_A_DAY);
	require(chron == 1581724800);
}

operation test_from_timestamp_pre_70s() {
	val datejust = t.from_timestamp(-1168128000);
	print("DATE", datejust);
	require(datejust.date == new_date(1932, 12, 26));
}

operation test_from_timestamp_middle_of_the_day() {
	val _timestamp = 1606233881; // 24/11/2020 16.04.41
	val chron = t.from_timestamp(_timestamp);

	expect_datejust(chron, new_datejust(new_date(2020, 11, 24), new_time(16, 04, 41)));
}

operation test_from_timestamp_era_4() {
	val datejust = t.from_timestamp(780364800);
	print("Date", datejust);
	expect_date(datejust.date, new_date(1994, 9, 24));
}

operation test_from_date() {
	val datejust = new_datejust(
		new_date(2020, 11, 24),
		new_time(16, 4, 41)
	);
	val expected_timestamp = 1606233881;
	val res_timestamp = t.to_timestamp(datejust);
	
	expect_timestamp(res_timestamp, expected_timestamp);
}

operation test_from_datejust_original_date(){
	val datejust = new_datejust(
		new_date(1970, 1, 1)
	);
	
	val expected = 0; 
	
	val res_timestamp = t.to_timestamp(datejust);
	expect_integer(res_timestamp, expected);
}

operation  test_original_weekday() {
	val datejust = new_datejust(
		new_date(1970, 1, 1)
	);
	
	val expected = 4; // it was a Thursday
	
	val res_weekday = t.get_weekday(datejust);
	
	expect_integer(res_weekday, expected);
}

operation test_gets_weekday() {
	val datejust = new_datejust(
		new_date(2020, 11, 24),
		new_time(16, 4, 41)
	);
	val expected = 2; // should be Tuesday
	
	val res_weekday = t.get_weekday(datejust);
	expect_integer(res_weekday, expected);
}

// TODO: test the weekday BEFORE 1970-01-01


